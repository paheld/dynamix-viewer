#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import sys
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dynamix.settings")

import dynamix.settings as settings

import daemon

import logging
#logging.basicConfig(level=settings.LOG_LEVEL)

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

import django
django.setup()

import django.core.handlers.wsgi

import tornado.httpserver
import tornado.ioloop
import tornado.wsgi
import tornado.web
import tornado.websocket
import json
from threading import Thread

import zmq

import tools.zmqbroker as zmqbroker
from tools.sanity import check_tasks
from time import sleep


class MyStaticHandler(tornado.web.StaticFileHandler):
    def get_content_type(self):
        if self.absolute_path.split(".")[-1] == "arff":
            return "text/plain"
        return tornado.web.StaticFileHandler.get_content_type(self)


def run():

    context = zmq.Context.instance()
    info_socket = context.socket(zmq.PUB)
    info_socket.bind("tcp://*:{}".format(settings.INFO_PORT))

    broker = Thread(target=zmqbroker.broker)
    broker.setDaemon(True)
    broker.start()

    check_tasks()

    try:
        wsgi_app = tornado.wsgi.WSGIContainer(django.core.handlers.wsgi.WSGIHandler())
        tornado_app = tornado.web.Application([
            (r'{}(.*)'.format(settings.STATIC_URL), MyStaticHandler, {'path': settings.STATIC_ROOT}),
            (r'{}(.*)'.format(settings.MEDIA_URL), MyStaticHandler, {'path': settings.MEDIA_ROOT}),
            ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
          ])
        server = tornado.httpserver.HTTPServer(tornado_app)

        server.listen(settings.TORNADO_PORT)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        info_socket.send(b"EXIT")


if __name__ == "__main__":
    if len(sys.argv) == 2:
        d = daemon.Daemon(settings.TORNADO_PIDFILE, run)
        daemon.deamon_manager(d)
    else:
        run()