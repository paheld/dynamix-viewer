#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import zmq

import sys
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dynamix.settings")

import dynamix.settings as settings

import daemon

import logging
#logging.basicConfig(level=settings.LOG_LEVEL)

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

import django
django.setup()

from threading import Thread
from time import sleep
import json

from django.core.serializers import serialize, deserialize

import cluster
from tools.partition import store_partition, analyze_partition, compare_partitions

try:
    from time import process_time as clock
except ImportError:
    from time import clock

from socket import gethostname
import traceback


def do_cluster(run, partition, graphinstance, experiment, **kwargs):

    print("Run Clustering:", run.pk)

    try:
        run.host = gethostname()
        function = cluster.algorithms.get(experiment.algorithm).get("function")
        try:
            params = json.loads(experiment.params)
        except (ValueError, TypeError):
            params = {}

        graph = graphinstance.load_graph()

        start = clock()
        part = function(graph, **params)
        end = clock()

        store_partition(part, graphinstance, partition)

        run.runtime = end - start
        run.success = True

    except Exception:
        run.success = False
        run.log = traceback.format_exc()
        print("EXCEPTION")
        print(run.log)
    return run


def do_scoring(partition, graphinstance):
    print("Score:", partition.id)
    part = partition.load_partition()
    graph = graphinstance.load_graph()

    partition = analyze_partition(graph, part, partition, False)

    return partition


def do_compare_partition(partition, partitioncomparision, **kwargs):
    print("Compare:",partition[0].id, partition[1].id)

    return compare_partitions(partition[0].load_partition(),
                              partition[1].load_partition(),
                              partitioncomparision)


def parse_objects(object_data):
    objects = dict()
    for obj in deserialize("xml", object_data):
        t = obj.object.__class__.__name__.lower()
        if t in objects:
            if isinstance(objects[t], list):
                objects[t].append(obj.object)
            else:
                objects[t] = [objects[t], obj.object]
        else:
            objects[t] = obj.object
    return objects


def do_task(data):
    if data.get("task") == "cluster":
        objects = parse_objects(data["objects"])
        return do_cluster(**objects)

    if data.get("task") == "score_partition":
        objects = parse_objects(data["objects"])
        return do_scoring(**objects)

    if data.get("task") == "compare_partition":
        objects = parse_objects(data["objects"])
        return do_compare_partition(**objects)

    print(data)


def worker():
    context = zmq.Context.instance()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://{}:{}".format(settings.SERVER_ADRESS, settings.BROKER_PORT))

    result = None
    while True:
        if not result:
            result = b""

        socket.send(result)

        workload = socket.recv()

        if workload:
            data = json.loads(workload.decode("utf-8"))
            result = do_task(data)
            if result:
                try:
                    result = serialize("xml", result)
                except TypeError:
                    result = serialize("xml", [result]).encode("utf-8")
        else:
            result = None
            sleep(15)


def run():
    broker = Thread(target=worker)
    broker.setDaemon(True)
    broker.start()

    context = zmq.Context.instance()
    info_socket = context.socket(zmq.SUB)
    info_socket.connect("tcp://{}:{}".format(settings.SERVER_ADRESS, settings.INFO_PORT))
    info_socket.setsockopt(zmq.SUBSCRIBE, b"")

    msg = ""
    while msg != b"EXIT":
        msg = info_socket.recv()


if __name__ == "__main__":
    if len(sys.argv) == 2:
        d = daemon.Daemon(settings.TORNADO_PIDFILE, run)
        daemon.deamon_manager(d)
    else:
        run()