#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dynamix.settings")
django.setup()

import dynamix.settings as settings

from viewer.models import *

import networkx as nx

import argparse

parser = argparse.ArgumentParser(description="Imports Graph Files")
parser.add_argument("filename", type=argparse.FileType("rb"), help="the graph file")
parser.add_argument("-n", "--name", help="the name of the graph")
parser.add_argument("-l", "--label", help="a graph label, if not given, the filename is used")
parser.add_argument("-d", "--desc", help="description")
parser.add_argument("-t", "--type", choices=["gml", "graphml", "gexf"], help="filetype")

args = parser.parse_args()

file = args.filename
fileparts = file.name.split(".")
ext = fileparts[-1]
filename = (".".join(fileparts[:-1])).split("/")[-1]

name = args.name if args.name else filename
label = args.label if args.label else filename

read_functions = {
    "gml": nx.read_gml,
    "graphml": nx.read_graphml,
    "gexf": nx.read_gexf,
}

type = args.type if args.type else ext

try:
    read_func = read_functions[type]
except IndexError:
    print("Please select filetype")
    exit(-1)

if MetaGraph.objects.filter(label=label).exists():
    print("Graph Label exist already, please chose another one.")
    exit(-2)

g = read_func(file)

try:
    os.makedirs("{}/graphs/{}/".format(settings.MEDIA_ROOT, label))
except FileExistsError:
    pass
nx.write_gpickle(g, "{}/graphs/{}/graph.pickle".format(settings.MEDIA_ROOT, label), protocol=2)
g_obj = MetaGraph.objects.create(name=name, label=label)
if args.desc:
    g_obj.desc = args.desc
g_obj.save()
gi_obj = GraphInstance.objects.create(graph=g_obj)
gi_obj.save()