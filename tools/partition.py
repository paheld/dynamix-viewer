#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

from viewer.models import *
from uuid import uuid1
import json
import os
import dynamix.settings
from community import modularity
import sklearn.metrics as metrics


def store_partition(partition, graph_instance, part_obj):
    if not part_obj:
        uuid = uuid1()
        part_obj = Partition.objects.create(uuid=uuid, graph=graph_instance)
        part_obj.save()

    with open(part_obj.get_filename(), "w") as f:
        json.dump(partition, f, sort_keys=True)

    return part_obj


def analyze_partition(graph, partition, partition_object, save=True):
    p = partition_object

    number_of_all_nodes = nx.number_of_nodes(graph)

    p.number_of_nodes = len(partition)

    com2nodes = {}
    for node, com in partition.items():
        if com not in com2nodes:
            com2nodes[com] = []
        com2nodes[com].append(node)

    p.number_of_communities = len(com2nodes)

    total_edges = nx.number_of_edges(graph)

    intra_cluster_edges = 0

    inner_graph = nx.create_empty_copy(graph)
    for n1, n2 in graph.edges_iter():
        try:
            if partition[n1] == partition[n2]:
                intra_cluster_edges += 1
                inner_graph.add_edge(n1, n2)
        except IndexError:
            pass

    possible_intra_cluster_edges = 0
    possible_inter_cluster_edges = 0

    div_adj = 1 if graph.is_directed() else 2

    nodes_inner_degree_over_median = 0

    out_degree_fractions = dict()

    for com_list in com2nodes.values():
        possible_intra_cluster_edges += len(com_list) * (len(com_list)-1) // div_adj
        possible_inter_cluster_edges += len(com_list) * (number_of_all_nodes - len(com_list)) // div_adj

        degrees = graph.degree(com_list)
        degrees_list = list(degrees.values())
        degrees_list.sort()
        median_degree = degrees_list[len(degrees_list)//2]

        nodes_inner_degree_over_median += len([nid
                                               for nid, deg in inner_graph.degree(com_list).items()
                                               if deg > median_degree])

        inner_degrees = inner_graph.degree(com_list)
        odf = {nid: float(inner_degrees[nid]) / degrees[nid] if degrees.get(nid, False) else 0 for nid in com_list}
        out_degree_fractions.update(odf)


    inter_cluster_edges = total_edges - intra_cluster_edges

    p.internal_density = float(intra_cluster_edges) / possible_intra_cluster_edges
    p.average_internal_degree = 2. * intra_cluster_edges / number_of_all_nodes

    p.edges_inside = intra_cluster_edges

    p.fraction_over_median_degree = float(nodes_inner_degree_over_median) / number_of_all_nodes

    p.triangle_participation_ratio = float(len([nid
                                                for nid, tri in nx.triangles(inner_graph).items()
                                                if tri > 0])) / number_of_all_nodes

    p.expansion = float(inter_cluster_edges) / number_of_all_nodes

    p.cut_ratio = float(inter_cluster_edges) / possible_inter_cluster_edges

    p.conductance = float(inter_cluster_edges) / (inter_cluster_edges + 2 * intra_cluster_edges)

    p.normalized_cut = float(inter_cluster_edges) / (2*intra_cluster_edges + inter_cluster_edges) + \
                       float(inter_cluster_edges) / (2*(total_edges-intra_cluster_edges) + inter_cluster_edges)

    odfs = list(out_degree_fractions.values())
    odfs.sort()

    p.maximum_out_degree_fraction = odfs[-1]
    p.average_out_degree_fraction = sum(odfs) / len(odfs)
    p.flake_out_degree_fraction = float(len([x for x in odfs if x > 0.5])) / len(odfs)

    p.modularity = modularity(partition, graph)

    if save:
        p.save()

    return p


def compare_partitions(p1, p2, result_object, save=True):

    keys = list(set(p1.keys()) & set(p2.keys()))
    label_1 = [p1[l] for l in keys]
    label_2 = [p2[l] for l in keys]

    result_object.intersection_size = len(keys)

    result_object.homogeneity_score, result_object.completeness_score, result_object.v_measure_score = \
        metrics.homogeneity_completeness_v_measure(label_1, label_2)

    result_object.adjusted_mutual_info_score = metrics.adjusted_mutual_info_score(label_1, label_2)
    result_object.adjusted_rand_score = metrics.adjusted_rand_score(label_1, label_2)
    result_object.mutual_info_score = metrics.mutual_info_score(label_1, label_2)
    result_object.normalized_mutual_info_score = metrics.normalized_mutual_info_score(label_1, label_2)

    if save:
        result_object.save()

    return result_object
