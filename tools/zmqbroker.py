#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import zmq
import dynamix.settings as settings
import json
from threading import Thread
from time import sleep

from django.core.serializers import deserialize


def enqueue(task):
    if isinstance(task, str):
        task = task.encode("utf-8")
    elif not isinstance(task, bytes):
        task = json.dumps(task).encode("utf-8")

    context = zmq.Context.instance()
    socket = context.socket(zmq.PUSH)
    socket.connect("inproc://new_tasks")
    socket.send(task)


def save_objects():
    context = zmq.Context.instance()
    socket = context.socket(zmq.PULL)
    socket.bind("inproc://store_objects")

    while True:
        data = socket.recv().decode("utf-8")
        for obj in deserialize("xml", data):
            obj.save()
            obj = obj.object._default_manager.get(pk=obj.object.pk)
            try:
                obj.process()
            except AttributeError:
                pass


def broker():
    obj_storage = Thread(target=save_objects)
    obj_storage.setDaemon(True)
    obj_storage.start()

    sleep(0.1)

    context = zmq.Context.instance()

    backend = context.socket(zmq.ROUTER)
    backend.bind("tcp://*:{}".format(settings.BROKER_PORT))

    frontend = context.socket(zmq.PULL)
    frontend.bind("inproc://new_tasks")

    object_store = context.socket(zmq.PUSH)
    object_store.connect("inproc://store_objects")

    backend_poller = zmq.Poller()
    backend_poller.register(backend, zmq.POLLIN)

    frontend_poller = zmq.Poller()
    frontend_poller.register(frontend, zmq.POLLIN)

    while True:
        socket, mode = backend_poller.poll()[0]

        if socket == backend and mode == zmq.POLLIN:
            address, _, data = backend.recv_multipart()

            if data:
                object_store.send(data)

            if frontend_poller.poll(100):
                task = frontend.recv()

                backend.send_multipart([
                    address,
                    b'',
                    task
                ])
            else:
                backend.send_multipart([
                    address,
                    b'',
                    b''
                ])