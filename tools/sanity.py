#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

from viewer.models import *


def check_tasks():
    for pc in PartitionComparision.objects.filter(intersection_size=None):
        pc.process()

    for gt in GroundTruth.objects.all():
        gt.process()

    for run in Run.objects.filter(success=None):
        run.process()

    for run in Run.objects.filter(success=True, number_of_nodes=None):
        run.process()


