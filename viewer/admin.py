from django.contrib import admin

from viewer.models import *

# Register your models here.

admin.site.register(GraphGroup)
admin.site.register(MetaGraph)
admin.site.register(GraphInstance)
admin.site.register(Partition)
admin.site.register(GroundTruth)
admin.site.register(Experiment)
admin.site.register(Run)
admin.site.register(PartitionComparision)
