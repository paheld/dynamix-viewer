# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GraphGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='GraphInstance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('params', models.TextField(blank=True, null=True)),
                ('label', models.CharField(default='graph', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='MetaGraph',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('label', models.CharField(max_length=255)),
                ('desc', models.TextField(blank=True, null=True)),
                ('params', models.TextField(blank=True, null=True)),
                ('group', models.ForeignKey(blank=True, null=True, to='viewer.GraphGroup')),
            ],
        ),
        migrations.AddField(
            model_name='graphinstance',
            name='graph',
            field=models.ForeignKey(to='viewer.MetaGraph'),
        ),
    ]
