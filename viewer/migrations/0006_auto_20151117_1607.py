# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0005_graphinstance_path_prefix'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='partitioncomparision',
            unique_together=set([('p1', 'p2')]),
        ),
    ]
