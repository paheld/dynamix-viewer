# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0006_auto_20151117_1607'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='experiment',
            options={'ordering': ('graph', 'algorithm')},
        ),
        migrations.AlterModelOptions(
            name='graphgroup',
            options={'ordering': ('order', 'name')},
        ),
        migrations.AlterModelOptions(
            name='graphinstance',
            options={'ordering': ('graph', 'label')},
        ),
        migrations.AlterModelOptions(
            name='groundtruth',
            options={'ordering': ('label',)},
        ),
        migrations.AlterModelOptions(
            name='metagraph',
            options={'ordering': ('group', 'name')},
        ),
        migrations.AlterModelOptions(
            name='run',
            options={'ordering': ('experiment', 'graph', 'number')},
        ),
        migrations.AddField(
            model_name='graphgroup',
            name='order',
            field=models.FloatField(default=0),
        ),
    ]
