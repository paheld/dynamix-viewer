# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0004_auto_20151116_1356'),
    ]

    operations = [
        migrations.AddField(
            model_name='graphinstance',
            name='path_prefix',
            field=models.CharField(default='', max_length=255, editable=False),
        ),
    ]
