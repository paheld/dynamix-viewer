# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0003_auto_20151111_1453'),
    ]

    operations = [
        migrations.CreateModel(
            name='Experiment',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('algorithm', models.CharField(max_length=255)),
                ('params', models.TextField(null=True, blank=True)),
                ('runs', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='GroundTruth',
            fields=[
                ('partition_ptr', models.OneToOneField(auto_created=True, primary_key=True, parent_link=True, to='viewer.Partition', serialize=False)),
                ('label', models.CharField(max_length=255, default='Ground Truth')),
                ('desc', models.TextField(null=True, blank=True)),
            ],
            bases=('viewer.partition',),
        ),
        migrations.CreateModel(
            name='PartitionComparision',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('intersection_size', models.IntegerField(null=True, blank=True)),
                ('adjusted_mutual_info_score', models.FloatField(null=True, blank=True)),
                ('adjusted_rand_score', models.FloatField(null=True, blank=True)),
                ('completeness_score', models.FloatField(null=True, blank=True)),
                ('homogeneity_score', models.FloatField(null=True, blank=True)),
                ('mutual_info_score', models.FloatField(null=True, blank=True)),
                ('normalized_mutual_info_score', models.FloatField(null=True, blank=True)),
                ('v_measure_score', models.FloatField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Run',
            fields=[
                ('partition_ptr', models.OneToOneField(auto_created=True, primary_key=True, parent_link=True, to='viewer.Partition', serialize=False)),
                ('number', models.IntegerField(default=0)),
                ('host', models.CharField(null=True, blank=True, max_length=255)),
                ('runtime', models.FloatField(null=True, blank=True)),
                ('log', models.TextField(null=True, blank=True)),
                ('success', models.NullBooleanField()),
                ('experiment', models.ForeignKey(to='viewer.Experiment')),
            ],
            bases=('viewer.partition',),
        ),
        migrations.AddField(
            model_name='partition',
            name='path_prefix',
            field=models.CharField(editable=False, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='graphinstance',
            name='label',
            field=models.CharField(editable=False, default='graph', max_length=255),
        ),
        migrations.AlterField(
            model_name='metagraph',
            name='label',
            field=models.CharField(unique=True, max_length=255, editable=False),
        ),
        migrations.AlterField(
            model_name='partition',
            name='uuid',
            field=models.UUIDField(unique=True, editable=False, default=uuid.uuid1),
        ),
        migrations.AlterUniqueTogether(
            name='graphinstance',
            unique_together=set([('graph', 'label')]),
        ),
        migrations.AddField(
            model_name='partitioncomparision',
            name='p1',
            field=models.ForeignKey(related_name='+', to='viewer.Partition'),
        ),
        migrations.AddField(
            model_name='partitioncomparision',
            name='p2',
            field=models.ForeignKey(related_name='+', to='viewer.Partition'),
        ),
        migrations.AddField(
            model_name='experiment',
            name='graph',
            field=models.ForeignKey(to='viewer.MetaGraph'),
        ),
    ]
