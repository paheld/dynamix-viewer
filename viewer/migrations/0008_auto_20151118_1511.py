# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0007_auto_20151118_1511'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='groundtruth',
            options={'ordering': ('graph', 'label')},
        ),
    ]
