# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('uuid', models.UUIDField(editable=False, unique=True)),
                ('number_of_nodes', models.IntegerField(null=True, blank=True)),
                ('number_of_communities', models.IntegerField(null=True, blank=True)),
                ('internal_density', models.FloatField(null=True, blank=True)),
                ('edges_inside', models.FloatField(null=True, blank=True)),
                ('average_internal_degree', models.FloatField(null=True, blank=True)),
                ('fraction_over_median_degree', models.FloatField(null=True, blank=True)),
                ('triangle_participation_ratio', models.FloatField(null=True, blank=True)),
                ('expansion', models.FloatField(null=True, blank=True)),
                ('cut_ratio', models.FloatField(null=True, blank=True)),
                ('conductance', models.FloatField(null=True, blank=True)),
                ('normalized_cut', models.FloatField(null=True, blank=True)),
                ('maximum_out_degree_fraction', models.FloatField(null=True, blank=True)),
                ('average_out_degree_fraction', models.FloatField(null=True, blank=True)),
                ('flake_out_degree_fraction', models.FloatField(null=True, blank=True)),
                ('modularity', models.FloatField(null=True, blank=True)),
                ('graph', models.ForeignKey(to='viewer.GraphInstance')),
            ],
        ),
    ]
