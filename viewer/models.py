from django.db import models

# Create your models here.
import networkx as nx
import os
import dynamix.settings as settings
from uuid import uuid1

from tools import zmqbroker

from django.core.serializers import serialize

import json

import traceback


class GraphGroup(models.Model):
    name = models.CharField(max_length=255)
    order = models.FloatField(default=0)

    def __str__(self):
        return "#{}: {}".format(self.id, self.name)

    class Meta:
        ordering = ("order", "name")


class MetaGraph(models.Model):
    name = models.CharField(max_length=255)
    label = models.CharField(max_length=255, unique=True, editable=False)
    desc = models.TextField(blank=True, null=True)
    params = models.TextField(blank=True, null=True)
    group = models.ForeignKey(GraphGroup, blank=True, null=True)

    def __str__(self):
        return "#{}: {} ({})".format(self.id, self.label, self.name)

    class Meta:
        ordering = ("group", "name")


class GraphInstance(models.Model):
    graph = models.ForeignKey(MetaGraph)
    params = models.TextField(blank=True, null=True)
    label = models.CharField(max_length=255, default="graph", editable=False)

    path_prefix = models.CharField(max_length=255, default="", editable=False)

    def update_path(self):
        self.path_prefix = "/graphs/{}/".format(self.graph.label)

    def load_graph(self):
        if not self.path_prefix:
            self.update_path()
            self.save()
        return nx.read_gpickle("{}/{}{}.pickle".format(settings.MEDIA_ROOT, self.path_prefix, self.label))

    def save(self, *args, **kwargs):
        self.update_path()
        models.Model.save(self, *args, **kwargs)

    def __str__(self):
        return "#{}: {}-{}".format(self.id, self.graph.label, self.label)

    class Meta:
        unique_together = (("graph", "label"),)
        ordering = ("graph", "label")


class Partition(models.Model):
    graph = models.ForeignKey(GraphInstance)
    uuid = models.UUIDField(unique=True, editable=False, default=uuid1)

    number_of_nodes = models.IntegerField(blank=True, null=True)
    number_of_communities = models.IntegerField(blank=True, null=True)

    internal_density = models.FloatField(blank=True, null=True)
    edges_inside = models.FloatField(blank=True, null=True)
    average_internal_degree = models.FloatField(blank=True, null=True)
    fraction_over_median_degree = models.FloatField(blank=True, null=True)
    triangle_participation_ratio = models.FloatField(blank=True, null=True)
    expansion = models.FloatField(blank=True, null=True)
    cut_ratio = models.FloatField(blank=True, null=True)
    conductance = models.FloatField(blank=True, null=True)
    normalized_cut = models.FloatField(blank=True, null=True)
    maximum_out_degree_fraction = models.FloatField(blank=True, null=True)
    average_out_degree_fraction = models.FloatField(blank=True, null=True)
    flake_out_degree_fraction = models.FloatField(blank=True, null=True)
    modularity = models.FloatField(blank=True, null=True)

    path_prefix = models.CharField(max_length=255, default="", editable=False)

    def update_path(self):
        try:
            self.path_prefix = "/graphs/{}/{}-partitions/".format(self.graph.graph.label, self.graph.label)
        except (GraphInstance.DoesNotExist):
            pass

    def process(self):
        others = list()

        try:
            run = self.run
            if run.success:
                if run.run == 0:
                    others.extend(Run.objects.filter(graph=self.graph, run=0, success=True).values_list("id", flat=True))
                others.extend(
                    Run.objects.filter(graph=self.graph, experiment=run.experiment, success=True).values_list("id", flat=True)
                )
            else:
                return
        except Run.DoesNotExist:
            pass

        try:
            self.groundtruth
            others.extend(GroundTruth.objects.filter(graph=self.graph).values_list("id", flat=True))
            others.extend(Run.objects.filter(graph=self.graph, success=True).values_list("id", flat=True))
        except GroundTruth.DoesNotExist:
            pass

        others.extend(GroundTruth.objects.filter(graph=self.graph).values_list("id", flat=True))

        others = set(others)
        others = others - set(PartitionComparision.objects.filter(p1=self).values_list("p2", flat=True))
        others = others - set(PartitionComparision.objects.filter(p2=self).values_list("p1", flat=True))
        try:
            others.remove(self.id)
        except KeyError:
            pass
        for elem in others:
            if elem < self.id:
                obj = PartitionComparision.objects.create(p1_id=elem, p2=self)
            else:
                obj = PartitionComparision.objects.create(p2_id=elem, p1=self)
            obj.save()
            obj.process()

    def save(self, *args, **kwargs):
        self.update_path()
        models.Model.save(self, *args, **kwargs)

    def get_filename(self):
        if not self.path_prefix:
            self.update_path()

        path = "{}{}".format(settings.MEDIA_ROOT, self.path_prefix)

        try:
            os.makedirs(path)
        except OSError:
            pass
        return "{}/{}.json".format(path, self.uuid.hex)

    def load_partition(self):
        with open(self.get_filename()) as f:
            part = json.load(f)
            part_new = {}
            for k,v in part.items():
                try:
                    part_new[int(k)] = v
                except ValueError:
                    part_new[k] = v
            return part_new


class GroundTruth(Partition):
    label = models.CharField(max_length=255, default="Ground Truth")
    desc = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ("graph", "label")


class Experiment(models.Model):
    algorithm = models.CharField(max_length=255)
    graph = models.ForeignKey(MetaGraph)
    params = models.TextField(null=True, blank=True)
    runs = models.IntegerField(default=1)

    def create_runs(self):
        for g in self.graph.graphinstance_set.all():
            for i in range(self.runs):
                obj, created = Run.objects.get_or_create(experiment=self, number=i, graph=g)

    def save(self, *args, **kwargs):
        models.Model.save(self, *args, **kwargs)
        self.create_runs()

    class Meta:
        ordering = ("graph", "algorithm")


class Run(Partition):
    experiment = models.ForeignKey(Experiment)
    number = models.IntegerField(default=0)
    host = models.CharField(max_length=255, null=True, blank=True)
    runtime = models.FloatField(null=True, blank=True)
    log = models.TextField(null=True, blank=True)
    success = models.NullBooleanField()

    def process(self):
        if self.success is None:
            task = {
                "task": "cluster",
                "objects": serialize("xml", [self, self.partition_ptr, self.experiment, self.graph]),
            }
            zmqbroker.enqueue(task)

        if self.success and not self.number_of_nodes:
            task = {
                "task": "score_partition",
                "objects": serialize("xml", [self.partition_ptr, self.graph])
            }
            zmqbroker.enqueue(task)

    def save(self, *args, **kwargs):
        Partition.save(self, *args, **kwargs)
        self.process()

    class Meta:
        ordering = ("experiment", "graph", "number")


class PartitionComparision(models.Model):
    p1 = models.ForeignKey(Partition, related_name="+")
    p2 = models.ForeignKey(Partition, related_name="+")

    intersection_size = models.IntegerField(null=True, blank=True)
    adjusted_mutual_info_score = models.FloatField(null=True, blank=True)
    adjusted_rand_score = models.FloatField(null=True, blank=True)
    completeness_score = models.FloatField(null=True, blank=True)
    homogeneity_score = models.FloatField(null=True, blank=True)
    mutual_info_score = models.FloatField(null=True, blank=True)
    normalized_mutual_info_score = models.FloatField(null=True, blank=True)
    v_measure_score = models.FloatField(null=True, blank=True)

    def process(self):
        if self.intersection_size is None:
            task = {
                "task": "compare_partition",
                "objects": serialize("xml", [self, self.p1, self.p2])
            }
            zmqbroker.enqueue(task)

    def __str__(self):
        return "{} - {}".format(self.p1_id, self.p2_id)

    class Meta:
        unique_together = (
            ("p1", "p2"),
        )
