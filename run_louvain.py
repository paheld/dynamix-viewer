#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dynamix.settings")
django.setup()

import dynamix.settings as settings

from viewer.models import *

import networkx as nx

import community

import argparse

from tools.partition import *

parser = argparse.ArgumentParser(description="Runs Louvain algorithm on a specific graph")
parser.add_argument("graph_instance", type=int, help="ID of selected graph")

args = parser.parse_args()

graph_instance = GraphInstance.objects.get(pk=args.graph_instance)
g = graph_instance.load_graph()

part = community.best_partition(g)

part_obj = store_partition(part, graph_instance)

analyze_partition(g, part, part_obj)